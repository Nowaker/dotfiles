#!/usr/bin/env ruby

require 'open3'

class KernelVersionChecker
  GREEN = "\e[32m"
  RED = "\e[31m"
  RESET = "\e[0m"

  def check_current_kernel
    running_kernel = get_running_kernel
    return unless running_kernel

    package = determine_package(running_kernel)
    installed = get_installed_version(package)
    return unless installed

    display_versions(package, running_kernel, installed)
  end

  private

  def get_running_kernel
    stdout, _, status = Open3.capture3('uname -r')
    return nil unless status.success?
    stdout.strip
  end

  def determine_package(kernel_version)
    # Extract everything after version-release as the kernel variant
    if kernel_version =~ /^\d+\.\d+\.\d+-\d+-(.+)$/
      "linux-#{$1}"
    else
      'linux'
    end
  end

  def get_installed_version(package)
    stdout, _, status = Open3.capture3("pacman -Q #{package}")
    return nil unless status.success?

    # Handle the special case where pacman output has an extra dot
    version = stdout.split[1].strip
    version.gsub('.arch', '-arch')
  end

  def display_versions(package, loaded, installed)
    puts "Package:   #{package}"

    if loaded == installed
      puts "Loaded:    #{GREEN}#{loaded}#{RESET}"
      puts "Installed: #{GREEN}#{installed}#{RESET}"
    else
      common_prefix = find_common_prefix(loaded, installed)
      diff_start = loaded.index(loaded[common_prefix.length..-1])

      puts "Loaded:    #{highlight_difference(loaded, diff_start)}"
      puts "Installed: #{highlight_difference(installed, diff_start)}"
    end
  end

  def find_common_prefix(str1, str2)
    i = 0
    while i < str1.length && i < str2.length && str1[i] == str2[i]
      i += 1
    end
    # Go back to the last dot to ensure we highlight complete version segments
    while i > 0 && str1[i-1] != '.'
      i -= 1
    end
    str1[0...i]
  end

  def highlight_difference(version, diff_start)
    return version if diff_start.nil?

    prefix = version[0...diff_start]
    remainder = version[diff_start..-1]

    "#{GREEN}#{prefix}#{RED}#{remainder}#{RESET}"
  end
end

if __FILE__ == $0
  checker = KernelVersionChecker.new
  checker.check_current_kernel
end