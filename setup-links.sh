#!/usr/bin/env bash

set -x

for dotfile in *rc git* ssh/*; do
  ln -sf "`realpath "$dotfile"`" ~/."$dotfile"
done

mkdir -p ~/bin
for binfile in bin/*; do
  ln -sf "`realpath "$binfile"`" ~/"$binfile"
done
