# general purpose
alias ls='ls --color'
alias 'cd..'='cd ..'
alias c='cd'

color() {
  grep --color -E "$1|$" $2
}

# secrets
if [ -f ~/.zshrc.secrets ]; then
  source ~/.zshrc.secrets
fi

# homebrew
export HOMEBREW_NO_ANALYTICS=1
export HOMEBREW_NO_ENV_HINTS=1

homebrew() {
  # load Linux brew on demand - don't pollute with outdated software by default
  export HOMEBREW_PREFIX="$HOME/.linuxbrew"
  export HOMEBREW_CELLAR="$HOMEBREW_PREFIX/Cellar"
  export HOMEBREW_REPOSITORY="$HOMEBREW_PREFIX/Homebrew"
  export PATH="$HOMEBREW_PREFIX/bin:$HOMEBREW_PREFIX/sbin:$PATH"
  export MANPATH="$HOMEBREW_PREFIX/share/man:$MANPATH"
  export INFOPATH="$HOMEBREW_PREFIX/share/info:$INFOPATH"
}

# mac
if [[ "$OSTYPE" == "darwin"* ]]; then
  I_USE_MACOS_BTW=1

  alias subl='open -a "Sublime Text"'

  if [ -f ~/.iterm2_shell_integration.zsh ]; then
    source ~/.iterm2_shell_integration.zsh
  fi

  if [ -d /opt/homebrew ]; then
    PREFIX='/opt/homebrew'
    SECONDPREFIX='/usr/local'
  else
    PREFIX='/usr/local'
  fi

  export PATH="$PREFIX/bin:$PREFIX/sbin:$PATH"
  if [ -v SECONDPREFIX ]; then
    export PATH="$PATH:$SECONDPREFIX/bin:$SECONDPREFIX/sbin"
  fi
  export PATH="$PATH:/opt/local/bin:/opt/local/sbin" # Macports
  export MANPATH="$PREFIX/opt/coreutils/libexec/gnuman:$MANPATH"
  export PATH="$PREFIX/opt/coreutils/libexec/gnubin:$PATH"
  export MANPATH="$PREFIX/opt/gnu-sed/libexec/gnuman:$MANPATH"
  export PATH="$PREFIX/opt/gnu-sed/libexec/gnubin:$PATH"
  export PATH="$PREFIX/opt/findutils/libexec/gnubin:$PATH"
  export MANPATH="$PREFIX/opt/findutils/libexec/gnuman:$MANPATH"
  export CODECONFIG="$HOME/Library/Application Support/Code/User"

  if [ -d /opt/homebrew/bin ]; then
    eval "$(/opt/homebrew/bin/brew shellenv)"
  fi

  # vscode
  export PATH="/Applications/Visual Studio Code.app/Contents/Resources/app/bin:$PATH"

  # rancher desktop
  export PATH="$HOME/.rd/bin:$PATH"

  # Use after capturing a video using Screenshot.app. Its files are too big.
  # Superseded by ffcapture()
  aa-compress() {
    local input_file="$1"
    local custom_prefix="$2"
    local input_dir=$(dirname "$(realpath "$input_file")")
    local file_name_only=$(basename "$input_file")
    local filename_no_ext="${file_name_only%.*}"

    local date_in_filename=$(echo "$filename_no_ext" | grep -oE '[0-9]{4}-[0-9]{2}-[0-9]{2}')
    if [[ -z "$date_in_filename" ]]; then
      echo "Error: Date not found in the file name: $filename_no_ext"
      return 1
    fi

    local output_file="${input_dir}/${date_in_filename} ${custom_prefix} ${filename_no_ext}.mp4"

    ffmpeg -i "$input_file" -vf "scale=iw/2:ih/2,fps=15" -c:v h264_videotoolbox -b:v 500k -c:a copy "$output_file"
  }

  alias pdftk='docker run --rm --volume "$(pwd)":/work pdftk/pdftk:latest'

  alias mac-restart-dhcp='sudo ipconfig set en0 BOOTP && sudo ipconfig set en0 DHCP'
fi

# arch linux
if [[ "$OSTYPE" == "linux"* ]]; then
  I_USE_ARCH_BTW=1

  alias open='kde-open'

  export PYTHON=/usr/bin/python

  if [ ! -f "$HOME/.Xmodmap" ]; then
    setxkbmap -option caps:none
  fi

  old-arch-kernel-is-current() {
    local loaded_version installed_version kernel_package kernel_variant
    loaded_version=$(uname -r)

    # Extract kernel variant (anything after the last hyphen, if it exists)
    kernel_variant=$(echo "$loaded_version" | grep -o '[^-]*$')
    if [[ "$kernel_variant" != "$loaded_version" && "$kernel_variant" != [0-9]* ]]; then
      kernel_package="linux-$kernel_variant"
    else
      kernel_package="linux"
    fi

    installed_version=$(pacman -Q "$kernel_package" | awk '{print $2}')
    [[ "$kernel_package" == "linux" ]] && installed_version=$(echo "$installed_version" | sed 's/\.arch/-arch/')

    if [[ "$loaded_version" == "$installed_version" ]]; then
      return 0
    else
      echo "Loaded:    $loaded_version"
      echo "Installed: $installed_version"
      return 1
    fi
  }
fi

# ruby
alias gi='gem install'
alias x='bundle exec'
alias rvminstall='CFLAGS="-O2 -fno-tree-dce -fno-optimize-sibling-calls" rvm install'
export BUNDLER_ARGS='-j30'
source ~/.rvm/scripts/rvm

# git
alias g='git'
alias st='git status'
alias clone='git clone'
alias gclone='git clone'
alias push='git push'
alias fpush='git push origin +master'
alias pull='git pull'
alias purr='git pull --rebase'
alias rebase='git rebase'
alias add='git add'
alias addu='git add -u'
alias unadd='git reset'
alias gd='git diff'
alias gdc='git diff --cached'
alias gc='git diff --cached'
alias gdiff='git diff'
alias gdiffcached='git diff --cached'
alias gcached='git diff --cached'
alias gpatch='git diff --cached > patch'
alias greset='git reset'
alias checkout='git checkout'
alias co='git checkout'
alias cob='git checkout -b'
alias commit='git commit'
alias ci='git commit'
alias ciam='git commit --amend'
alias gciam='git commit --amend'
alias gcam='git commit --amend'
alias gcim='git commit --amend'
alias gciamdate='git commit --amend --date="$(date --rfc-2822)"'
alias uncommit='git reset --soft HEAD^'
alias uci='git reset --soft HEAD^'
alias unci='git reset --soft HEAD^'
alias l='git log'
alias glog='git log'
alias remote='git remote'
alias grv='git remote -v'
alias re='git remote'
alias branch='git branch'
alias me='git merge'
alias merge='git merge'
alias show='git show'
alias stash='git stash'
alias greset='git reset'
alias gr='git reset'

# git remote set-url origin [url]
# git remote set-url [name] [url]
grsu() {
  if [ $# -eq 1 ]; then
    git remote set-url origin "$1"
  elif [ $# -eq 2 ]; then
    git remote set-url "$1" "$2"
  else
    echo "Usage: gitu <url> or gitu <name> <url>"
  fi
}
alias remote-set-url='grsu'
remote-add-origin() {
  local project
  project=$(basename "$PWD" | tr -d '\n' | tr -c '[:alnum:]' '-')
  git remote add origin "git@gitlab.com:Nowaker/${project}.git"
}

clone-gitlab() {
  local project
  local target
  if [ $# -eq 0 ]; then
    project=$(basename "$PWD" | tr -d '\n' | tr -c '[:alnum:]' '-')
    target='.'
  else
    project="$1"
    target="$project"
  fi

  git clone "git@gitlab.com:Nowaker/${project}.git" "$target"
  [ $? -eq 0 ] && cd "$project"
}

gitrack() {
  BRANCH="$(git rev-parse --abbrev-ref HEAD 2>/dev/null)"
  [ "$BRANCH" = "HEAD" ] && BRANCH=master
  git branch "--set-upstream-to=origin/$BRANCH" "$BRANCH"
}

git-reauthor() {
  NAME=$(git config user.name)
  EMAIL=$(git config user.email)
  git commit --amend --author="$NAME <$EMAIL>"
}

alias pusha='git-push-as'
git-push-as() {
  BRANCH=$(git rev-parse --abbrev-ref HEAD)
  git push origin "$BRANCH":"$1"
}

# node
function loadnode() {
  if [ -f /usr/share/nvm/init-nvm.sh ]; then
    source /usr/share/nvm/init-nvm.sh
  elif [ -f /opt/homebrew/opt/nvm/nvm.sh ]; then
    source /opt/homebrew/opt/nvm/nvm.sh
    unset PREFIX
    nvm use default
  elif [ -f /usr/local/opt/nvm/nvm.sh ]; then
    source /usr/local/opt/nvm/nvm.sh
    nvm use --delete-prefix --default 12
  fi
}

# zsh
export PATH="$HOME/bin:$PATH"
export HISTSIZE=100000
export SAVEHIST=100000
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY

# gcp
if [ -d "$HOME/google-cloud-sdk/bin" ]; then
  export PATH="$PATH:$HOME/google-cloud-sdk/bin"
fi

# host
export IDN_DISABLE=0

# rsync
export CVS_RSH="ssh"

# libvirt
export VIRSH_DEFAULT_CONNECT_URI=qemu:///system
export LIBVIRT_DEFAULT_URI=qemu:///system

# go
export GOPATH="$HOME/projects/go"
export PATH="$PATH:$GOPATH/bin"
export gopath="$GOPATH"

# python
export PATH="$HOME/.local/bin/:$PATH"

# terraform
alias tf='terraform'
alias ti='terraform init'
alias tp='terraform plan'
alias ta='terraform apply'
alias tay='terraform apply -auto-approve'
treapply() {
  tf taint "$1";
  ta -target="$1";
}
alias td='terraform destroy'
alias tfer='terraformer'
export TF_PLUGIN_CACHE_DIR="$HOME/.terraform.d/plugin-cache"
export TF_CLI_ARGS_backup=-

# chef
alias knife-share='x knife supermarket share ${PWD##*/} -o ..'

# docker
alias dc='docker-compose'

# kubernetes
alias pods="kubectl get pods"
alias podsa="kubectl get pods --all-namespaces"

# ffmpeg
alias ffinfo='ffprobe -v error -show_entries format=size,duration:stream=codec_name,codec_type,width,height'

ffcapture () {
  local devices_output=$(ffmpeg -f avfoundation -list_devices true -i "" 2>&1)
  local screen_capture_id=$(echo "$devices_output" | ggrep -Po '\[\K\d+(?=] Capture screen)')

  if [[ -z "$screen_capture_id" ]]; then
    echo "No screen capture device found."
    return 1
  fi

  echo "Screen capture device ID: $screen_capture_id"

  for i in {3..1}; do
    echo "Starting in $i..."
    sleep 1
  done

  echo "Starting now!"
  local datetime_in_filename=$(date "+%Y-%m-%d %H:%M:%S")
  local custom_prefix="$1"
  local output_file="${datetime_in_filename} ${custom_prefix}.mp4"

  if [[ "$OSTYPE" == "darwin"* ]]; then
    ffmpeg -f avfoundation -framerate 30 -i "$screen_capture_id" -vf "scale=iw/2:ih/2,fps=15" -c:v h264_videotoolbox -b:v 500k -c:a copy "$output_file"
  else
    echo "Not implemented for Linux yet"
    exit 1
  fi
}

alias ffrecord=ffcapture

# dpiconvert 300 file.pdf             => file-300dpi.pdf
# dpiconvert 300 file.pdf output.pdf  => output.pdf
# dpiconvert half file.pdf            => file-xxx.pdf
dpiconvert() {
  local dpi input_file output_file final_dpi

  if [[ $# -lt 2 ]]; then
    echo "Usage: dpiconvert <dpi or word 'half'> <input file> [optional output file]"
    return 1
  fi

  dpi=$1
  input_file=$2

  if [[ $dpi == "half" ]]; then
    if [[ $input_file =~ ([0-9]+)dpi ]]; then
      final_dpi=$((${match[1]} / 2))
    else
      echo "Could not determine DPI from input file name. Specify DPI manually."
      return 1
    fi
  else
    final_dpi=$dpi
  fi

  if [[ -z $3 ]]; then
    if [[ $input_file =~ ([0-9]+)dpi ]]; then
      output_file="${input_file/${match[1]}dpi/${final_dpi}dpi}"
    else
      output_file="${input_file%.*}-${final_dpi}dpi.${input_file##*.}"
    fi
  else
    output_file=$3
  fi

  echo gs -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress \
     -dCompatibilityLevel=1.4 \
     -dDownsampleColorImages=true -dColorImageResolution=$final_dpi \
     -dDownsampleGrayImages=true -dGrayImageResolution=$final_dpi \
     -dDownsampleMonoImages=true -dMonoImageResolution=$final_dpi \
     -dNOPAUSE -dBATCH \
     -sOutputFile="$output_file" "$input_file"
}

# gcloud
export CLOUDSDK_PYTHON=/usr/bin/python

# dreamhost
export PATH="$HOME/projects/dreamhost/kubekick:$PATH"

# rust
export PATH="$HOME/.cargo/bin:$PATH"
