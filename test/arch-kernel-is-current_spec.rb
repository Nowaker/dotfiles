#!/usr/bin/env ruby

require 'bundler/inline'
gemfile do
  source 'https://rubygems.org'
  gem 'rspec'
end

require 'rspec/autorun'
require_relative '../bin/arch-kernel-is-current'

RSpec.describe KernelVersionChecker do
  let(:checker) { KernelVersionChecker.new }

  describe '#check_current_kernel' do
    context 'with standard kernel' do
      before do
        # Mock uname -r to return standard kernel
        allow(Open3).to receive(:capture3).with('uname -r')
          .and_return(['6.13.3-arch1-1', '', double(success?: true)])

        # Mock pacman query for standard kernel
        allow(Open3).to receive(:capture3).with('pacman -Q linux')
          .and_return(['linux 6.13.3.arch1-1', '', double(success?: true)])
      end

      it 'shows matching versions in green' do
        green = KernelVersionChecker::GREEN
        reset = KernelVersionChecker::RESET

        expect { checker.check_current_kernel }.to output(
          "Package:   linux\n" \
          "Loaded:    #{green}6.13.3-arch1-1#{reset}\n" \
          "Installed: #{green}6.13.3-arch1-1#{reset}\n"
        ).to_stdout
      end

      context 'when versions mismatch' do
        before do
          allow(Open3).to receive(:capture3).with('pacman -Q linux')
            .and_return(['linux 6.13.4.arch1-1', '', double(success?: true)])
        end

        it 'highlights the differing version segments' do
          green = KernelVersionChecker::GREEN
          red = KernelVersionChecker::RED
          reset = KernelVersionChecker::RESET

          expect { checker.check_current_kernel }.to output(
            "Package:   linux\n" \
            "Loaded:    #{green}6.13.#{red}3-arch1-1#{reset}\n" \
            "Installed: #{green}6.13.#{red}4-arch1-1#{reset}\n"
          ).to_stdout
        end
      end
    end

    context 'with variant kernel' do
      before do
        # Mock uname -r to return variant kernel
        allow(Open3).to receive(:capture3).with('uname -r')
          .and_return(['6.12.12-1-CUSTOM', '', double(success?: true)])

        # Mock pacman query for variant kernel
        allow(Open3).to receive(:capture3).with('pacman -Q linux-CUSTOM')
          .and_return(['linux-CUSTOM 6.12.13-1', '', double(success?: true)])
      end

      it 'highlights version differences' do
        green = KernelVersionChecker::GREEN
        red = KernelVersionChecker::RED
        reset = KernelVersionChecker::RESET

        expect { checker.check_current_kernel }.to output(
          "Package:   linux-CUSTOM\n" \
          "Loaded:    #{green}6.12.#{red}12-1-CUSTOM#{reset}\n" \
          "Installed: #{green}6.12.#{red}13-1#{reset}\n"
        ).to_stdout
      end
    end
  end

  describe '#determine_package' do
    it 'determines package name from kernel version' do
      expect(checker.send(:determine_package, '6.13.3-arch1-1')).to eq('linux')
      expect(checker.send(:determine_package, '6.12.12-1-CUSTOM')).to eq('linux-CUSTOM')
    end
  end

  describe '#find_common_prefix' do
    it 'finds the common version prefix up to the differing segment' do
      prefix = checker.send(:find_common_prefix, '6.13.2-arch1-1', '6.13.3-arch1-1')
      expect(prefix).to eq('6.13.')
    end
  end

  describe '#highlight_difference' do
    it 'applies correct color codes to version segments' do
      green = KernelVersionChecker::GREEN
      red = KernelVersionChecker::RED
      reset = KernelVersionChecker::RESET

      highlighted = checker.send(:highlight_difference, '6.13.2-arch1-1', 5)
      expect(highlighted).to eq("#{green}6.13.#{red}2-arch1-1#{reset}")
    end
  end
end